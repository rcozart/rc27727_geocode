package ee461l_homework4.homework4_new;

import android.annotation.TargetApi;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MapsActivity extends FragmentActivity {

    private Geocoder geocoder;


    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onClick (View view) throws IOException {

        EditText text = (EditText) findViewById(R.id.editText);
        String location = text.getText().toString();

        System.out.println("I");

        JSONObject resp;

        try{
            System.out.println(location);
            System.out.println("am");
            String myRequest = formatMyRequest(location);
            //geocoder = new Geocoder(this);
            System.out.println("here");

            String resultString = "";

            try
            {
                URL url = new URL(myRequest);
                // Read all the text returned by the server
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String str;

                while ((str = in.readLine()) != null)
                {
                    // str is one line of text; readLine() strips the newline character(s)
                    // You can use the contain method here.
                    resultString+= str;
                    System.out.println(resultString);
                }
                in.close();

                resp = new JSONObject(resultString);
                JSONArray respArray = resp.getJSONArray("results");
                resp = respArray.getJSONObject(0);
                resp = resp.getJSONObject("geometry");
                resp = resp.getJSONObject("location");



                Double lat = resp.getDouble("lat");
                Double lng = resp.getDouble("lng");
                System.out.println(lat.toString());

                LatLng myLocation = new LatLng(lat,lng);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 13));

                mMap.addMarker(new MarkerOptions().title("here's where you want to see").position(myLocation));
                CameraUpdate center = CameraUpdateFactory.newLatLng(myLocation);
                mMap.moveCamera(center);
                mMap.getMyLocation();
            } catch (MalformedURLException e) {
            } catch (IOException e) {
            }catch (JSONException e){
                System.out.println("JSON EXCEPTION");
                e.printStackTrace();

            }




        }catch(Exception e){
            System.out.print("Unknown Exception");
            e.printStackTrace();
        }
    }

    public String formatMyRequest(String input) {
        String out = "https://maps.googleapis.com/maps/api/geocode/json?address=";
        String[] items = input.split(" ");
        for (int i = 0; i < items.length; i++) {
            if (i < items.length - 1) {
                out += items[i] + "+";
            } else {
                out += items[i];
            }
        }
        //out += "&key=AIzaSyAF4yz2R5CNf7vvmF75CBJSpqo7YlKzA1s";
        return out;
    }

}
